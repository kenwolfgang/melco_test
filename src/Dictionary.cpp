#include <fstream>
#include "Dictionary.h"
#include "SpellCheck.h"
#include<bits/stdc++.h>

Dictionary::Dictionary(std::string fileName) {
	std::ifstream fs(fileName);
	std::vector<std::string>* wordList = new std::vector<std::string>();

	std::string word;
	while(std::getline(fs, word)) {
		wordList->push_back(word);
		KnownWordsList.push_back(word);		// Also build up dictionary global.

	}

	AddKnownWords(*wordList);
}

void Dictionary::AddKnownWords(std::vector<std::string> words) {
	// TODO: Fill in logic
}

bool Dictionary::IsKnownWord(std::string word) const {
	bool Found = false;
	int i;

	// Search through global dictionary for desired word.
	for (i = 0; i < KnownWordsList.size(); i++) {
		std::string DictWord = KnownWordsList.at(i);
    std::string SearchWord = word;
    // If the word in the dictinary is not capitalized, then it is not a proper
		// name and the desired word should be converted to lower case.
		if ((DictWord[0] >= 'a') && (DictWord[0] <= 'z')) {
			transform(SearchWord.begin(), SearchWord.end(), SearchWord.begin(), ::tolower);
		}
		// If the word matches this dictionary word.
		if (SearchWord.compare(KnownWordsList.at(i)) == 0) {
			Found = true;		// Indicate that we did find the word.
			break;					// Break out of search loop.
		}
	}
	return Found;
}

Dictionary &Dictionary::Instance() {
	static Dictionary dict("words.txt");
	return dict;
}

std::vector<std::string> Dictionary::KnownWords(const std::string word, size_t WithinRange) {
  std::vector<std::string> ListInRange;
	// Search through dictionary for words that are within the desired range.
	for (int i = KnownWordsList.size() - 1; i >= 0; i--) {
		// If this dictionary word distance is within desired range.
		if (SpellCheck::Distance(word, KnownWordsList.at(i)) <= WithinRange) {
			ListInRange.push_back(KnownWordsList.at(i)); // Store word in vector string.
		}
	}
	return(ListInRange);
}
