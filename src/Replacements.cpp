#include "SpellCheck.h"
#include "Dictionary.h"

namespace SpellCheck {
	std::vector<std::string> Replacements(const std::string& word, size_t maxDistance) {
		return(Dictionary::Instance().KnownWords(word, maxDistance));
	}
}
