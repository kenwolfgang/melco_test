#include "SpellCheck.h"

namespace SpellCheck {
	size_t Distance(const std::string &testWord, const std::string &refWord) {
		    size_t testLen = testWord.size(); // Length of test word.
				size_t refLen = refWord.size();		// Length of reference word.
				size_t Dist;											// Variable to hold difference.

        // If size of test word is larger than reference word, re-call subroutine
				// with parameters reversed.
				if (testWord.size() > refWord.size() ) {
					Dist = Distance(refWord, testWord);
				}
				// If words are equal, then distance is 0.
				else if (testWord.compare(refWord) == 0) {
					Dist = 0;
				}
				// If test word length is 0, then distance is the length of reference word.
			  else if (testLen == 0) {
					Dist = refLen;
				}
				// Start distance calculation.
				else {
					std::vector<size_t> DistVect(testLen + 1);
					std::vector<size_t> AboveVect(testLen + 1);
					size_t cost;
					size_t cell_above;

          // Initialize distance vector.
					for (size_t i = 0; i <= testLen; ++i) {
						DistVect[i] = i;
					}
					// Go through each row of matrix.
					for (size_t j = 1; j <= refLen; ++j) {
						AboveVect = DistVect;	// Save off previous row
						DistVect[0]++;				// Increment column 0 to next row.
						// Go through columns of this row.
						for (size_t i = 1; i <= testLen; ++i) {
							// Calculate cost of this element in matrix.
							cost = (testWord[i - 1] == refWord[j -1 ]) ? 0 : 1;
							// Calculate value of this element in matrix.
							DistVect[i] = std::min(std::min((AboveVect[i] + 1), (DistVect[i - 1] + 1)), (AboveVect[i - 1] + cost));
						}
					}
					Dist = DistVect[testLen];	// Final distance.
				}
		return Dist;
	}
}
